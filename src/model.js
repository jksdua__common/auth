'use strict';

var merge = require('lodash.merge');
var zxcvbn = require('zxcvbn2');

var constants = require('./constants');

var tokenSchema = { type: 'string', minLength: constants.TOKEN_LENGTH, maxLength: constants.TOKEN_LENGTH, required: true };

module.exports = function(opt) {
  var MIN_PASSWORD_LENGTH = opt.password.length.min;
  var MAX_PASSWORD_LENGTH = opt.password.length.max;
  var MIN_PASSWORD_ENTROPY = opt.password.entropy.min;

  var rtn = {};

  var passSchema = {
    type: 'string',
    minLength: MIN_PASSWORD_LENGTH,
    maxLength: MAX_PASSWORD_LENGTH,
    required: true,
    validate: function(ins) {
      if (ins) {
        var res = zxcvbn(ins);
        if (res.entropy < MIN_PASSWORD_ENTROPY) {
          return 'is a weak password';
        }
      }
    }
  };
  function validateEmailPassPair(obj) {
    if (obj.email === obj.pass) {
      return 'cannot have the same email and password';
    }
  }

  rtn.schema = {
    type: 'object',
    required: true,
    properties: {
      _id: { required: true },
      status: { type: 'string', required: true, enum: constants.STATUSES },
      email: { type: 'string', format: 'email', required: true },
      hash: { type: 'string' },
      activationToken: merge({}, tokenSchema, { required: false }),
      resetToken: merge({}, tokenSchema, { required: false }),
      resetExpiry: { required: false },
      properties: { type: 'object', required: false },
      roles: {
        type: 'array',
        required: true,
        minItems: 1,
        items: { type: 'string', required: true }
      }
    },
    password: passSchema,
    validateEmailPassPair: validateEmailPassPair
  };

  return rtn;
};