'use strict';

var merge = require('lodash.merge');
var crypto = require('crypto');
var semver = require('semver');
var assert = require('http-assert');
var thunkify = require('thunkify');
var template = require('lodash.template');

var bcrypt;
try {
	bcrypt = require('bcrypt');
} catch(e) {
	bcrypt = require('bcryptjs');
}

var hash = thunkify(bcrypt.hash);
var compare = thunkify(bcrypt.compare);
var randomBytes = thunkify(crypto.randomBytes);

var constants = require('./constants');
var ROLE = constants.ROLE;
var STATUS = constants.STATUS;
var MESSAGE = constants.MESSAGE;
var TOKEN_LENGTH = constants.TOKEN_LENGTH;
var CURRENT_VERSION = constants.CURRENT_VERSION;
var RESET_TOKEN_EXPIRY = constants.RESET_TOKEN_EXPIRY;
var EMPTY_PASSWORD_HASH = constants.EMPTY_PASSWORD_HASH;

function debug(name) {
	return require('debug')('@knoxxnxt/auth:' + name);
}

// assert if the current version matches the semver version passed
var debugIsSupported = debug('isSupported');
function isSupported(pattern) {
	debugIsSupported('pattern: %s, current version: %s', pattern, CURRENT_VERSION);
	return semver.satisfies(CURRENT_VERSION, pattern);
}

var debug_Cleansed = debug('_cleansed');
function cleanse(user) {
	debug_Cleansed('user: %j', user);

	if (!user) { return; }

	var cleansed = {
		_id: user._id,
		email: user.email,
		roles: user.roles,
		status: user.status,
		properties: user.properties
	};

	debug_Cleansed('cleaned user: %j', cleansed);

	return cleansed;
}

function toStatus(result) {
	return { success: !!result };
}

var defaults = {
	mail: {
		templates: { dir: __dirname + '/mail/templates' },
		from: 'Default From <default@example.com>',
		url: {
			invite: {
				accept: 'http://example.com/auth/invite/accept?token=<%- user.activationToken %>&email=<%- user.email %>',
				reject: 'http://example.com/auth/invite/reject?token=<%- user.activationToken %>&email=<%- user.email %>'
			},
			activate: 'http://example.com/auth/activate?token=<%- user.activationToken %>&email=<%- user.email %>',
			reset: 'http://example.com/auth/reset?token=<%- user.resetToken %>&email=<%- user.email %>'
		},
		subject: {
			invite: 'You are invited',
			activate: 'Activate your account',
			reset: 'Reset Your account password'
		}
	},
	password: {
		rounds: 10,
		length: { min: 8, max: 100 },
		entropy: { min: 24 }
	},
	default: {
		properties: {},
		roles: []
	}
};

var debugInstance = debug('instance');

module.exports = function(opt) {
	opt = opt || {};
	opt.db = opt.db || require('./memorydb');
	opt.mail = merge({}, defaults.mail, opt.mail);
	opt.default = merge({}, defaults.default, opt.default);
	opt.password = merge({}, defaults.password, opt.password);

	if (!opt.mail.transport) {
		opt.mail.transport = require('nodemailer-stub-transport')();
		console.warn('Using stub mail transport. Emails will not be sent.');
	}

	var model = require('./model')(opt);
	var userSchema = model.schema;

	var mailOptionsSchema = { type: 'object', required: false };

	// compile mail url and subject templates
	var mailUrl = opt.mail.url;
	mailUrl.invite.accept = template(mailUrl.invite.accept);
	mailUrl.invite.reject = template(mailUrl.invite.reject);
	mailUrl.activate = template(mailUrl.activate);
	mailUrl.reset = template(mailUrl.reset);

	var mailSubject = opt.mail.subject;
	mailSubject.invite = template(mailSubject.invite);
	mailSubject.activate = template(mailSubject.activate);
	mailSubject.reset = template(mailSubject.reset);

	var db = opt.db;
	var mail = require('@knoxxnxt/mail')(opt.mail);

	var api = {
		// options passed to the factory
		options: opt,
		opt: opt,
		// user datastore
		db: db,
		// bcrypt hash thunk
		hash: hash,
		// bcrypt compare thunk
		compare: compare,
		// crypto.randomBytes thunk
		randomBytes: randomBytes,
		// auth constants
		constants: constants,
		// user cleanser - removes sensitive info such as hashes and one time tokens
		cleanse: cleanse,
		// mail instance
		mail: mail,
		// asserts if the current version is compatible with the semver version passed
		isSupported: isSupported,
		// current version
		version: CURRENT_VERSION,
		VERSION: CURRENT_VERSION
	};

	debugInstance('using defaults: %j', defaults);
	debugInstance('created with options: %j', opt);

	api.admin = function(options) {
		return require('./admin')(api, options);
	};

	var methods = api.methods = {};

	var debug_Get = debug('_get');
	function get(args) {
		debug_Get('called with args: %j', args);
		return db.findByEmail(args.email);
	}

	var debug_Insert = debug('_insert');
	function *insert(args) {
		debug_Insert('called with args: %j', args);

		var user = merge({
			activationToken: (yield randomBytes(TOKEN_LENGTH / 2)).toString('hex')
		}, args);

		if (args.pass) {
			// avoid bug where args.pass is copied onto the user object
			user.pass = void 0;
			user.hash = yield hash(args.pass, opt.password.rounds);
		}

		// status may be specified when inviting or doing a raw insert
		if (!user.status) {
			user.status = user.hash ? STATUS.REGISTERED : STATUS.INVITED;
		}

		user.roles = [ROLE.USER]
			.concat(opt.default.roles)
			.concat(args.roles || []);

		user.properties = merge({}, opt.default.properties, args.properties);

		yield db.insert(user);
		assert(user._id, 'Data store must set the `_id` field');

		debug_Insert('inserting user: %j', user);

		return user;
	}

	var debugGet = debug('get');
	methods.get = function *(args) {
		debugGet('called with args: %j', args);

		var user = yield get(args);
		assert(user, 400, MESSAGE.get.INVALID_EMAIL);

		debugGet('got user: %j', user);

		return cleanse(user);
	};
	merge(methods.get, {
		schema: {
			properties: {
				email: userSchema.properties.email
			},
			additionalProperties: false
		}
	});

	var debugAll = debug('all');
	methods.all = function *(args) {
		debugAll('called with args: %j', args);

		var users = (yield db.find(args)).map(cleanse);

		debugAll('got users: %j', users);

		return users;
	};
	merge(methods.all, {
		schema: {
			required: false,
			properties: {},
			additionalProperties: false
		}
	});

	var debugInsert = debug('insert');
	methods.insert = function *(args) {
		debugInsert('called with args: %j', args);

		var existing = yield db.findByEmail(args.email);
		assert(!existing, 400, MESSAGE.insert.DUPLICATE_EMAIL);

		var defaults = {
			status: STATUS.ENABLED
		};

		var user = yield insert(merge({}, defaults, args));

		debugInsert('inserted user: %j', user);

		return user;
	};
	merge(methods.insert, {
		schema: {
			properties: {
				email: userSchema.properties.email,
				pass: userSchema.password,
				roles: merge({}, userSchema.properties.properties, { required: false }),
				properties: merge({}, userSchema.properties.properties, { required: false })
			},
			validate: userSchema.validateEmailPassPair
		}
	});

	var debugUpdate = debug('update');
	methods.update = function *(args) {
		debugUpdate('called with args: %j', args);

		delete args._originalEmail;

		var res = toStatus(yield db.update(args));

		debugUpdate('res: %j', res);

		return res;
	};
	merge(methods.update, {
		schema: {
			properties: {},
			additionalProperties: true
		}
	});

	var debugRemove = debug('remove');
	methods.remove = function *(args) {
		debugRemove('called with args: %j', args);

		var res = toStatus(yield db.removeByEmail(args.email));

		debugRemove('remove status: %j', res);

		return res;
	};
	merge(methods.remove, {
		schema: {
			properties: {
				email: userSchema.properties.email
			},
			additionalProperties: false
		}
	});

	var debugLogin = debug('login');
	methods.login = function *(args) {
		debugLogin('called with args: %j', args);

		var user = yield db.findByEmail(args.email);

		// backwards compatibility to avoid breaking changes
		user = user || (yield db.findByEmail(args._originalEmail));

		debugLogin('retrieved user: %j', user);

		// hash regardless of whether user exists to avoid timing attacks
			// '' (empty string) is fine as a default since it will never be a valid user password
		var hash = user && user.hash || EMPTY_PASSWORD_HASH;
		var same = yield compare(args.pass, hash);

		assert(user && same, 400, MESSAGE.login.INVALID_USERNAME_PASSWORD);

		return cleanse(user);
	};
	merge(methods.login, {
		schema: {
			properties: {
				email: userSchema.properties.email,
				pass: userSchema.password
			},
			additionalProperties: false
		}
	});

	var debugRegister = debug('register');
	methods.register = function *(args) {
		debugRegister('called with args: %j', args);

		var existing = yield db.findByEmail(args.email);
		assert(!existing, 400, MESSAGE.register.DUPLICATE_EMAIL);

		var user = yield insert(args);

		debugRegister('inserted user: %j', user);

		var mailOptions = merge({
			template: {
				name: 'activate',
				locals: {
					user: user,
					url: opt.mail.url.activate({ user: user })
				}
			},
			mail: {
				from: opt.mail.from,
				to: user.email,
				subject: opt.mail.subject.activate({ user: user })
			}
		}, args.mailOptions);
		yield api.mail.send(mailOptions);

		debugRegister('sending mail with options: %j', mailOptions);

		return cleanse(user);
	};
	merge(methods.register, {
		schema: {
			properties: {
				email: userSchema.properties.email,
				pass: userSchema.password,
				roles: merge({}, userSchema.properties.roles, { required: false }),
				properties: merge({}, userSchema.properties.properties, { required: false }),
				mailOptions: mailOptionsSchema
			},
			additionalProperties: false,
			validate: userSchema.validateEmailPassPair
		}
	});

	var debugResendActivationEmail = debug('resendActivationEmail');
	methods.resendActivationEmail = function *(args) {
		debugResendActivationEmail('called with args: %j', args);

		var user = yield db.findByEmail(args.email);
		assert(user, 400, MESSAGE.resendActivationEmail.INVALID_EMAIL);
		assert.equal(user.status, STATUS.REGISTERED, 400, MESSAGE.resendActivationEmail.INVALID_STATE);

		debugResendActivationEmail('got user: %j', user);

		// update activation token
		merge(user, {
			activationToken: (yield randomBytes(TOKEN_LENGTH / 2)).toString('hex')
		});
		yield db.update(user);

		var mailOptions = merge({
			template: {
				name: 'activate',
				locals: {
					user: user,
					url: opt.mail.url.activate({ user: user })
				}
			},
			mail: {
				from: opt.mail.from,
				to: user.email,
				subject: opt.mail.subject.activate({ user: user })
			}
		}, args.mailOptions);
		var res = yield api.mail.send(mailOptions);

		debugResendActivationEmail('sending mail with options: %j', mailOptions);

		return toStatus(res);
	};
	merge(methods.resendActivationEmail, {
		schema: {
			properties: {
				email: userSchema.properties.email,
				mailOptions: mailOptionsSchema
			},
			additionalProperties: false
		}
	});

	var debugActivate = debug('activate');
	methods.activate = function *(args) {
		debugActivate('called with args: %j', args);

		var user = yield db.findByEmail(args.email);
		assert(user, 400, MESSAGE.activate.INVALID_EMAIL);
		assert.equal(user.status, STATUS.REGISTERED, 400, MESSAGE.activate.INVALID_STATE);

		debugActivate('got user: %j', user);

		if (!args.skipTokenVerification) {
			debugActivate('performing token verification');
			// ensure token is valid
			assert(user.activationToken === args.token, 400, MESSAGE.activate.INVALID_TOKEN);
		} else {
			debugActivate('skipping token verification');
		}

		// all good, update status and return user
		user.status = STATUS.ENABLED;
		user.activationToken = null;
		yield db.update(user);

		debugActivate('user activated and updated: %j', user);

		return cleanse(user);
	};
	merge(methods.activate, {
		schema: {
			properties: {
				email: userSchema.properties.email,
				token: merge({}, userSchema.properties.activationToken, { required: false }),
				skipTokenVerification: { type: 'boolean', required: false, default: false }
			},
			additionalProperties: false
		}
	});

	var debugInvite = debug('invite');
	methods.invite = function *(args) {
		debugInvite('called with args: %j', args);

		var existing = yield db.findByEmail(args.email);
		assert(!existing, 400, MESSAGE.invite.DUPLICATE_EMAIL);

		var user = yield insert(args);

		debugInvite('inserted user: %j', user);

		var mailOptions = merge({
			template: {
				name: 'invite',
				locals: {
					user: user,
					url: {
						accept: opt.mail.url.invite.accept({ user: user }),
						reject: opt.mail.url.invite.reject({ user: user })
					}
				}
			},
			mail: {
				from: opt.mail.from,
				to: user.email,
				subject: opt.mail.subject.invite({ user: user })
			}
		}, args.mailOptions);
		yield api.mail.send(mailOptions);

		debugInvite('sent mail with options: %j', mailOptions);

		return cleanse(user);
	};
	merge(methods.invite, {
		schema: {
			properties: {
				email: userSchema.properties.email,
				roles: merge({}, userSchema.properties.roles, { required: false }),
				properties: merge({}, userSchema.properties.properties, { required: false })
			},
			additionalProperties: false
		}
	});

	var debugResendInvitationEmail = debug('resendInvitationEmail');
	methods.resendInvitationEmail = function *(args) {
		debugResendInvitationEmail('called with args: %j', args);

		var user = yield db.findByEmail(args.email);
		assert(user, 400, MESSAGE.resendInvitationEmail.INVALID_EMAIL);
		assert.equal(user.status, STATUS.INVITED, 400, MESSAGE.resendInvitationEmail.INVALID_STATE);

		debugResendInvitationEmail('got user: %j', user);

		var mailOptions = merge({
			template: {
				name: 'invite',
				locals: {
					user: user,
					url: {
						accept: opt.mail.url.invite.accept({ user: user }),
						reject: opt.mail.url.invite.reject({ user: user })
					}
				}
			},
			mail: {
				from: opt.mail.from,
				to: user.email,
				subject: opt.mail.subject.invite({ user: user })
			}
		}, args.mailOptions);
		var res = yield api.mail.send(mailOptions);

		debugResendInvitationEmail('sent mail with options: %j', mailOptions);

		return toStatus(res);
	};
	merge(methods.resendInvitationEmail, {
		schema: {
			properties: {
				email: userSchema.properties.email,
				mailOptions: mailOptionsSchema
			},
			additionalProperties: false
		}
	});

	var debugAcceptInvite = debug('acceptInvite');
	methods.acceptInvite = function *(args) {
		debugAcceptInvite('called with args: %j', args);

		var user = yield db.findByEmail(args.email);
		assert(user, 400, MESSAGE.acceptInvite.INVALID_EMAIL);
		assert.equal(user.status, STATUS.INVITED, 400, MESSAGE.acceptInvite.INVALID_STATE);

		debugAcceptInvite('got user: %j', user);

		if (!args.skipTokenVerification) {
			debugAcceptInvite('doing token verification');
			// ensure token is valid
			assert(user.activationToken === args.token, 400, MESSAGE.acceptInvite.INVALID_TOKEN);
		} else {
			debugAcceptInvite('skipping token verification');
		}

		// all good, update status and return user
		user.hash = yield hash(args.pass, opt.password.rounds);
		user.status = STATUS.ENABLED;
		user.activationToken = null;
		yield db.update(user);

		debugAcceptInvite('user activated and updated: %j', user);

		return cleanse(user);
	};
	merge(methods.acceptInvite, {
		schema: {
			properties: {
				email: userSchema.properties.email,
				pass: userSchema.password,
				token: merge({}, userSchema.properties.activationToken, { required: false }),
				skipTokenVerification: { type: 'boolean', required: false, default: false }
			},
			additionalProperties: false,
			validate: userSchema.validateEmailPassPair
		}
	});

	var debugRejectInvite = debug('rejectInvite');
	methods.rejectInvite = function *(args) {
		debugRejectInvite('called with args: %j', args);

		var user = yield db.findByEmail(args.email);
		assert(user, 400, MESSAGE.rejectInvite.INVALID_EMAIL);
		assert.equal(user.status, STATUS.INVITED, 400, MESSAGE.rejectInvite.INVALID_STATE);

		debugRejectInvite('got user: %j', user);

		if (!args.skipTokenVerification) {
			debugRejectInvite('doing token verification');
			// ensure token is valid
			assert(user.activationToken === args.token, 400, MESSAGE.rejectInvite.INVALID_TOKEN);
		} else {
			debugRejectInvite('skipping token verification');
		}

		debugRejectInvite('removing user permanently');
		return toStatus(yield db.removeByEmail(args.email));
	};
	merge(methods.rejectInvite, {
		schema: {
			properties: {
				email: userSchema.properties.email,
				token: merge({}, userSchema.properties.activationToken, { required: false }),
				skipTokenVerification: { type: 'boolean', required: false, default: false }
			},
			additionalProperties: false
		}
	});

	var debugGetProperties = debug('getProperties');
	methods.getProperties = function *(args) {
		debugGetProperties('called with args: %j', args);

		var user = yield get(args);
		assert(user, 400, MESSAGE.properties.INVALID_USER_ID);

		debugGetProperties('got user: %j', user);

		return user.properties;
	};
	merge(methods.getProperties, {
		schema: {
			properties: {
				email: userSchema.properties.email
			},
			additionalProperties: false
		}
	});

	var debugSetProperties = debug('setProperties');
	methods.setProperties = function *(args) {
		debugSetProperties('called with args: %j', args);

		var user = yield get(args);
		assert(user, 400, MESSAGE.properties.INVALID_USER_ID);

		debugSetProperties('got user: %j', user);

		user.properties = args.properties;
		return toStatus(yield db.update(user));
	};
	merge(methods.setProperties, {
		schema: {
			properties: {
				email: userSchema.properties.email,
				properties: userSchema.properties.properties
			},
			additionalProperties: false
		}
	});

	var debugChangePassword = debug('changePassword');
	methods.changePassword = function *(args) {
		debugChangePassword('called with args: %j', args);

		var user = yield get(args);
		assert(user, 400, MESSAGE.changePassword.INVALID_USER_ID);

		var same = yield compare(args.oldPass, user.hash);
		assert(same, 400, MESSAGE.changePassword.INVALID_CURRENT_PASSWORD);

		user.hash = yield hash(args.newPass, opt.password.rounds);
		var res = toStatus(yield db.update(user));

		debugChangePassword('user updated: %j', res);

		return res;
	};
	merge(methods.changePassword, {
		schema: {
			properties: {
				email: userSchema.properties.email,
				oldPass: userSchema.password,
				newPass: userSchema.password
			},
			additionalProperties: false
		}
	});

	var debugResetRequest = debug('resetRequest');
	methods.resetRequest = function *(args) {
		debugResetRequest('called with args: %j', args);

		var user = yield db.findByEmail(args.email);
		assert(user, 400, MESSAGE.resetRequest.INVALID_EMAIL);

		user.resetToken = (yield randomBytes(TOKEN_LENGTH / 2)).toString('hex');
		user.resetExpiry = new Date(Date.now() + RESET_TOKEN_EXPIRY);

		var updated = yield db.update(user);
		assert(updated, 'Record not updated');

		debugResetRequest('user updated: %j', user);

		var mailOptions = merge({
			template: {
				name: 'reset',
				locals: {
					user: user,
					url: opt.mail.url.reset({ user: user })
				}
			},
			mail: {
				from: opt.mail.from,
				to: user.email,
				subject: opt.mail.subject.reset({ user: user })
			}
		}, args.mailOptions);
		var res = yield api.mail.send(mailOptions);

		debugResetRequest('sent mail with options: %j', mailOptions);

		return toStatus(res);
	};
	merge(methods.resetRequest, { schema: methods.get.schema }, {
		schema: {
			properties: {
				mailOptions: mailOptionsSchema
			}
		}
	});

	var debugResetResponse = debug('resetResponse');
	methods.resetResponse = function *(args) {
		debugResetResponse('called with args: %j', args);

		var user = yield db.findByEmail(args.email);
		assert(user, 400, MESSAGE.resetResponse.INVALID_EMAIL);

		debugResetResponse('got user: %j', user);

		if (!args.skipTokenVerification) {
			debugResetResponse('doing token verification');
			// ensure token is valid
			assert(user.resetToken === args.token, 400, MESSAGE.resetResponse.INVALID_TOKEN);
		} else {
			debugResetResponse('skipping token verification');
		}

		// ensure token is not expired
		assert(user.resetExpiry > new Date(), 400, MESSAGE.resetResponse.EXPIRED_TOKEN);

		// all good, set the new password
		user.hash = yield hash(args.pass, opt.password.rounds);
		user.resetToken = user.resetExpiry = null;
		var res = yield db.update(user);

		debugResetResponse('updated user: %j', user);

		return toStatus(res);
	};
	merge(methods.resetResponse, {
		schema: {
			properties: {
				email: userSchema.properties.email,
				pass: userSchema.password,
				token: merge({}, userSchema.properties.resetToken, { required: false }),
				skipTokenVerification: { type: 'boolean', required: false, default: false }
			},
			additionalProperties: false,
			validate: userSchema.validateEmailPassPair
		}
	});

	var debugDisable = debug('disable');
	methods.disable = function *(args) {
		debugDisable('called with args: %j', args);

		var user = yield get(args);
		user.status = STATUS.DISABLED;

		debugDisable('user updated: %j', user);

		return toStatus(yield db.update(user));
	};
	merge(methods.disable, { schema: methods.get.schema });

	var debugEnable = debug('enable');
	methods.enable = function *(args) {
		debugEnable('called with args: %j', args);

		var user = yield get(args);
		user.status = STATUS.ENABLED;

		debugEnable('user updated: %j', user);

		return toStatus(yield db.update(user));
	};
	merge(methods.enable, { schema: methods.get.schema });

	// wrappers
	var slice = Array.prototype.slice;
	function wrap(obj, key, fns) {
		let origMethod = obj[key];

		obj[key] = function *wrapper() {
			var args = slice.call(arguments);

			for (let fn of fns) {
				yield fn.apply(null, args);
			}

			return yield origMethod.apply(obj, args);
		};

		merge(obj[key], origMethod);
	}

	function *lowercaseEmail(args) {
		if (args.email) {
			args._originalEmail = args.email;
			args.email = args.email.toLowerCase();
		}
	} // jshint ignore:line

	for (var methodName in methods) {
		wrap(methods, methodName, [lowercaseEmail]);
	}

	debugInstance('returning api: %j', api);

	return api;
};
