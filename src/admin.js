'use strict';

var co = require('co');
var net = require('net');
var repl = require('repl');
var assign = require('lodash.assign');
var assert = require('assert');

var debug = require('debug')('@knoxxnxt/auth:admin');

function sendResult(socket) {
  return function(data) {
    return socket.write(JSON.stringify(data), 'utf8');
  };
}

function sendError(socket) {
  return function(err) {
    socket.write(err.stack, 'utf8');
  };
}

function replMethod(method, socket) {
  return function() {
    var args = arguments;

    co(function *() {
      return yield method.apply(null, args);
    }).then(sendResult(socket)).catch(sendError(socket));
  };
}

function createReplContext(api, socket) {
  var context = assign({}, api);

  for (var i in api.methods) {
    context.methods[i] = replMethod(api.methods[i], socket);
  }

  return context;
}

/**

  Creates an admin repl on a network socket or port

  @param {Object}     api                    Auth api
  @param {Object}     replOptions            REPL options passed to `repl.start`
    @param {Number}   [replOptions.port]     Port to listen on
    @param {Number}   [replOptions.host]     Host to listen on
    @param {Number}   [replOptions.sock]     Socket to listen on

 */
module.exports = function(api, replOptions) {
  assert('object' === typeof api, 'Invalid api');
  assert(
    replOptions && (replOptions.sock || replOptions.port),
    'Invalid repl options'
  );

  // create repl for each client
  var server = net.createServer(function(socket) {
    var socketReplOptions = assign(
      { prompt: 'auth > ' },
      replOptions,
      { input: socket, output: socket }
    );

    debug('creating repl for socket: %j with options: %j', socket.address(), socketReplOptions);

    var socketRepl = repl.start(socketReplOptions);
    socketRepl.on('exit', socket.end);

    var replContext = createReplContext(api, socket);
    replContext.auth = replContext;
    assign(socketRepl.context, replContext);
  }).listen(replOptions.sock || replOptions.port, replOptions.host);

  server.on('listening', function() {
    var str;

    if (replOptions.sock) {
      str = replOptions.sock;
    } else {
      str = replOptions.host || '0.0.0.0:';
      str += replOptions.port;
    }

    console.info('auth admin console listening on %s', str);
  });

  return { server: server };
};