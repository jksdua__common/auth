'use strict';

console.warn('Using memory db. User data will be cleared when the program exits');

var store = [];

// mimic async
function tick() {
  return process.nextTick;
}

// id function exposed for creating invalid ids in tests
exports.id = function() {
  return store.length + 100;
};

exports.find = function *() {
  yield tick();
  return store;
};

exports.findById = function *(id) {
  yield tick();
  return store[id - 1] || null;
};

exports.findByEmail = function *(email) {
  yield tick();

  return store.filter(function(record) {
    return email === record.email;
  })[0];
};

exports.insert = function *(user) {
  yield tick();

  delete user.pass;
  user._id = store.length + 1;

  store.push(user);

  return true;
};

exports.bulkInsert = function *(users) {
  return !!(yield (users.map(exports.insert)));
};

exports.update = function *(user) {
  yield tick();

  if (store[user._id - 1]) {
    store[user._id - 1] = user;
    return true;
  } else {
    return false;
  }
};

exports.removeByEmail = function *(email) {
  yield tick();

  var lengthBefore = store.length;

  store = store.filter(function(item) {
    return item.email !== email;
  });

  return store.length !== lengthBefore;
};

exports.reset = function *() {
  yield tick();
  store = [];
};

// for debugging
exports._store = store;