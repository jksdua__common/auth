'use strict';

exports.TOKEN_LENGTH = 64;

exports.RESET_TOKEN_EXPIRY = 1000 * 60 * 60 * 3; // 3 hours

exports.EMPTY_PASSWORD_HASH = '$2a$10$FISKBraDkuYqs5WNoyJiWe4.kgdJC1hHF0toHCzN80ZNLIRDKUBFS'; // password -> ''

exports.CURRENT_VERSION = require('../package.json').version;

exports.ROLE = {
  USER: 'user',
  USER_ADMIN: 'user admin'
};
exports.ROLES = ['user', 'user admin'];

exports.STATUS = {
  INVITED: 'invited',
  REGISTERED: 'registered',
  ENABLED: 'enabled',
  DISABLED: 'disabled'
};
exports.STATUSES = ['registered', 'enabled', 'disabled'];

exports.MESSAGE = {
  get: {
    INVALID_EMAIL: 'Invalid email'
  },
  insert: {
    DUPLICATE_EMAIL: 'Email address has already been registered'
  },
  login: {
    INVALID_USERNAME_PASSWORD: 'Invalid username/password'
  },
  register: {
    DUPLICATE_EMAIL: 'Email address has already been registered'
  },
  activate: {
    INVALID_EMAIL: 'User not found',
    INVALID_TOKEN: 'Invalid token',
    INVALID_STATE: 'Your account has already been activated'
  },
  resendActivationEmail: {
    INVALID_EMAIL: 'User not found',
    INVALID_STATE: 'Your account has already been activated'
  },
  invite: {
    DUPLICATE_EMAIL: 'Email address has already been registered'
  },
  resendInvitationEmail: {
    INVALID_EMAIL: 'User not found',
    INVALID_STATE: 'Your account has already been activated'
  },
  acceptInvite: {
    INVALID_EMAIL: 'User not found',
    INVALID_TOKEN: 'Invalid token',
    INVALID_STATE: 'Your account has already been activated'
  },
  rejectInvite: {
    INVALID_EMAIL: 'User not found',
    INVALID_TOKEN: 'Invalid token',
    INVALID_STATE: 'Your account has already been activated'
  },
  properties: {
    INVALID_USER_ID: 'User not found'
  },
  changePassword: {
    INVALID_USER_ID: 'User not found',
    INVALID_CURRENT_PASSWORD: 'Invalid current password'
  },
  resetRequest: {
    INVALID_EMAIL: 'User not found'
  },
  resetResponse: {
    INVALID_EMAIL: 'User not found',
    INVALID_TOKEN: 'Invalid token',
    EXPIRED_TOKEN: 'Expired token'
  }
};
