'use strict';

const PORT = 4000;

var helpers = require(__dirname + '/helpers');

helpers.resetDb(function(err) {
  if (err) { throw err; }

  helpers.loadFixtures(function(err) {
    if (err) { throw err; }

    helpers.auth.admin({ port: PORT });
  });
});