'use strict';

var co = require('co');
var koa = require('koa-framework');
var assert = require('assert');

// ==== simple koa app ====
function methodToRoute(app, router, obj, methodName) {
	var method = obj[methodName];
	router.post('/' + methodName, app.schema({ body: method.schema }), function *() {
		this.body = yield method(this.request.body);
	});
	// helper for test assertions
	app.route = app.route || {};
	app.route[methodName] = { url: 'http://' + app.host + ':' + app.port + '/' + methodName, method: 'post' };
}

var auth = require(__dirname + '/../src')({
	db: process.env.DB_MODULE ? require(process.env.DB_MODULE) : null
});

var app = koa();
app.env = process.env.NODE_ENV || 'test';
app.port = process.env.PORT || 3000;
app.host = process.env.HOST || 'localhost';

var router = app.router();
app.mount(router);

for (var i in auth.methods) {
	methodToRoute(app, router, auth.methods, i);
}

app.listen(app.port, app.host);


// ==== dummy mail ====
function spyOnAsyncMethod(object, methodName) {
	var original = object[methodName];
	var history = [];

	// replacement function
	object[methodName] = function() {
		var params = { req: arguments, res: null };

		// replace callback so we can record response received
		var done = arguments[arguments.length - 1];
		assert('function' === typeof done, 'Invalid async method - last argument should be a callback function');

		arguments[arguments.length - 1] = function() {
			params.res = arguments;
			history.push(params);

			// call original callback
			done.apply(null, arguments);
		};

		// call the original function
		original.apply(object, arguments);
	};

	return {
		history: history,
		restore: function() {
			object[methodName] = original;
		}
	};
}


// ==== database helpers + sample dummy data ====
function resetDb(done) {
	co(function *() {
		yield auth.db.reset();
	}).then(done, done);
}

function dummyUsers() {
	return [{
		email: 'test@jksdua.asia',
		// I like big hashes and I cannot lie
		hash: '$2a$10$jmUBONzZVmZoWh7GFhIeC.RiDnf.9rRExVO8ActVwdmgKTeJkeODy',
		roles: ['user'],
		status: 'enabled',
		properties: {
			name: { first: 'Test', last: 'User' },
			some: { other: 'property' },
			// for testing purposes
			__pass: 'I like big hashes and I cannot lie'
		}
	}];
}
function loadFixtures(done) {
	var users = dummyUsers();

	co(function *() {
		yield auth.db.bulkInsert(users);
	}).then(done, done);

	return users;
}


exports.db = auth.db;
exports.app = app;
exports.auth = auth;
exports.resetDb = resetDb;
exports.dummyUsers = dummyUsers;
exports.loadFixtures = loadFixtures;
exports.spyOnAsyncMethod = spyOnAsyncMethod;