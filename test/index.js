/* globals describe, beforeEach, it */

'use strict';

describe('#auth', function() {
	var co = require('co');
	var merge = require('lodash.merge');
	var expect = require('chai').expect;
	var request = require('request');
	var thunkify = require('thunkify');

	var requestThunk = thunkify(request);

	var helpers = require(__dirname + '/helpers');
	var db = helpers.db;
	var app = helpers.app;
	var auth = helpers.auth;
	var route = app.route;

	// users inserted
	var users;

	beforeEach(helpers.resetDb);
	beforeEach(function(done) {
		users = helpers.loadFixtures(done);
	});

	function dummyUser() {
		// return cloned copy
			// note: this process will convert the objectid to a string
		return JSON.parse(JSON.stringify(users[0]));
	}

	function cleansedDummyUser() {
		return auth.cleanse(dummyUser());
	}

	describe('#isSupported', function() {
		it('should not be supported', function() {
			var semver = '>99';

			expect(auth.isSupported(semver)).to.equal(false);
		});

		it('should be supported', function() {
			var current = auth.version;
			var semver = '~' + current.substring(0, current.length - 1) + 'x';

			expect(auth.isSupported(semver)).to.equal(true);
		});
	});

	describe('#methods', function() {
		describe('#get', function() {
			it('should work', function(done) {
				request({
					url: route.get.url,
					method: route.get.method,
					json: { email: dummyUser().email }
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.body).to.eql(cleansedDummyUser());

					done();
				});
			});
		});

		describe('#all', function() {
			it('should work', function(done) {
				request({
					url: route.all.url,
					method: route.all.method,
					json: {}
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.body).to.eql([cleansedDummyUser()]);

					done();
				});
			});
		});

		describe('#insert', function() {
			it('should fail if email already exists', function(done) {
				request({
					url: route.insert.url,
					method: route.insert.method,
					json: { email: dummyUser().email, pass: dummyUser().properties.__pass }
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.statusCode).to.equal(400);
					expect(res.body).to.have.property('error', auth.constants.MESSAGE.insert.DUPLICATE_EMAIL);

					done();
				});
			});

			it('should work', function(done) {
				request({
					url: route.insert.url,
					method: route.insert.method,
					json: { email: dummyUser().email + 'a', pass: dummyUser().properties.__pass }
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.statusCode).to.equal(200);

					request({
						url: route.get.url,
						method: route.get.method,
						json: { email: dummyUser().email + 'a' }
					}, function(err, res) {
						expect(err).to.not.exist; // jshint ignore:line
						expect(res.statusCode).to.equal(200);
						expect(res.body).to.have.property('email', dummyUser().email + 'a');
						expect(res.body).to.have.property('status', helpers.auth.constants.STATUS.ENABLED);

						done();
					});
				});
			});

			it('should support default roles', function(done) {
				var defaultRoles = auth.opt.default.roles;
				auth.opt.default.roles = ['bla'];

				request({
					url: route.insert.url,
					method: route.insert.method,
					json: { email: dummyUser().email + 'a', pass: dummyUser().properties.__pass }
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.statusCode).to.equal(200);

					request({
						url: route.get.url,
						method: route.get.method,
						json: { email: dummyUser().email + 'a' }
					}, function(err, res) {
						expect(err).to.not.exist; // jshint ignore:line
						expect(res.statusCode).to.equal(200);
						expect(res.body).to.have.property('email', dummyUser().email + 'a');
						expect(res.body).to.have.property('status', helpers.auth.constants.STATUS.ENABLED);
						expect(res.body.roles).to.contain('bla');

						auth.opt.default.roles = defaultRoles;
						done();
					});
				});
			});

			it('should support default properties', function(done) {
				var defaultProperties = auth.opt.default.properties;
				auth.opt.default.properties = { a: 'a' };

				request({
					url: route.insert.url,
					method: route.insert.method,
					json: { email: dummyUser().email + 'a', pass: dummyUser().properties.__pass }
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.statusCode).to.equal(200);

					request({
						url: route.get.url,
						method: route.get.method,
						json: { email: dummyUser().email + 'a' }
					}, function(err, res) {
						expect(err).to.not.exist; // jshint ignore:line
						expect(res.statusCode).to.equal(200);
						expect(res.body).to.have.property('email', dummyUser().email + 'a');
						expect(res.body).to.have.property('status', helpers.auth.constants.STATUS.ENABLED);
						expect(res.body).to.have.deep.property('properties.a', 'a');

						auth.opt.default.properties = defaultProperties;
						done();
					});
				});
			});
		});

		describe('#update', function() {
			it('should fail if user id is invalid', function(done) {
				var user = dummyUser();
				user._id = auth.db.id();

				request({
					url: route.update.url,
					method: route.update.method,
					json: user
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.statusCode).to.equal(200);
					expect(res.body).to.have.property('success', false);

					done();
				});
			});

			it('should work', function(done) {
				var user = dummyUser();
				user.properties.bla = 'bla';

				request({
					url: route.update.url,
					method: route.update.method,
					json: user
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.statusCode).to.equal(200);

					co(function *() {
						var dbUser = yield auth.db.findById(user._id);
						dbUser = JSON.parse(JSON.stringify(dbUser));
						expect(dbUser).to.eql(user);
					}).then(done, done);
				});
			});
		});

		describe('#remove', function() {
			it('should work', function(done) {
				request({
					url: route.remove.url,
					method: route.remove.method,
					json: { email: dummyUser().email }
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.statusCode).to.equal(200);

					request({
						url: route.get.url,
						method: route.get.method,
						json: { email: dummyUser().email }
					}, function(err, res) {
						expect(err).to.not.exist; // jshint ignore:line
						expect(res.statusCode).to.equal(400);

						done();
					});
				});
			});

			it('should fail if email doesnt exist', function(done) {
				request({
					url: route.remove.url,
					method: route.remove.method,
					json: { email: dummyUser().email + 'a', pass: dummyUser().properties.__pass }
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.statusCode).to.equal(400);

					done();
				});
			});
		});

		describe('#login', function() {
			it('should fail if email doesnt match', function(done) {
				request({
					url: route.login.url,
					method: route.login.method,
					json: { email: dummyUser().email + 'a', pass: dummyUser().properties.__pass }
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.statusCode).to.equal(400);
					expect(res.body).to.have.property('error', auth.constants.MESSAGE.login.INVALID_USERNAME_PASSWORD);

					done();
				});
			});

			// should fail on jsonschema
			it('should fail if no password is given', function(done) {
				request({
					url: route.login.url,
					method: route.login.method,
					json: { email: dummyUser().email }
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.statusCode).to.equal(400);

					done();
				});
			});

			it('should fail if password doesnt match', function(done) {
				request({
					url: route.login.url,
					method: route.login.method,
					json: { email: dummyUser().email, pass: dummyUser().properties.__pass + 'a' }
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.statusCode).to.equal(400);
					expect(res.body).to.have.property('error', auth.constants.MESSAGE.login.INVALID_USERNAME_PASSWORD);

					done();
				});
			});

			it('should return user if email and password match', function(done) {
				request({
					url: route.login.url,
					method: route.login.method,
					json: { email: dummyUser().email, pass: dummyUser().properties.__pass }
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.statusCode).to.equal(200);
					expect(res.body).to.eql(cleansedDummyUser());

					done();
				});
			});

			it('should return user if email and password match', function(done) {
				request({
					url: route.login.url,
					method: route.login.method,
					json: { email: dummyUser().email.toUpperCase(), pass: dummyUser().properties.__pass }
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.statusCode).to.equal(200);
					expect(res.body).to.eql(cleansedDummyUser());

					done();
				});
			});
		});

		describe('#register', function() {
			it('should fail if email is not valid', function(done) {
				var args = { email: 'abc', pass: '324vsxsdfsdfs' };

				request({
					url: route.register.url,
					method: route.register.method,
					json: args
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.statusCode).to.equal(400);
					//expect(res.body.error).to.equal();

					done();
				});
			});

			it('should not allow duplicate emails', function(done) {
				var args = { email: dummyUser().email, pass: 'woot woot password 12345678' };

				request({
					url: route.register.url,
					method: route.register.method,
					json: args
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.statusCode).to.equal(400);
					expect(res.body.error).to.equal(auth.constants.MESSAGE.register.DUPLICATE_EMAIL);

					done();
				});
			});

			it('should fail if password < 8 characters', function(done) {
				var args = { email: dummyUser().email, pass: 'abc' };

				request({
					url: route.register.url,
					method: route.register.method,
					json: args
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.statusCode).to.equal(400);
					//expect(res.body.error).to.equal();

					done();
				});
			});

			it('should fail if password does not meet minimum password entropy', function(done) {
				var args = { email: dummyUser().email, pass: 'password12345678' };

				request({
					url: route.register.url,
					method: route.register.method,
					json: args
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.statusCode).to.equal(400);
					expect(res.body.validationErrors[0].message).to.contain('weak password');
					done();
				});
			});

			it('should register user', function(done) {
				var args = { email: dummyUser().email + 'a', pass: 'crazy ass passpharse' };
				var mailSpy = helpers.spyOnAsyncMethod(helpers.auth.options.mail.transport, 'send');

				request({
					url: route.register.url,
					method: route.register.method,
					json: args
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.statusCode).to.equal(200);
					expect(mailSpy.history).to.have.length(1);
					expect(mailSpy.history[0].res[1].envelope.from).to.equal('default@example.com');
					expect(mailSpy.history[0].res[1].envelope.to).to.eql(['test@jksdua.asiaa']);
					var rawEmail = mailSpy.history[0].res[1].response.toString();
					expect(rawEmail).to.contain('Subject: Activate your account');
					expect(rawEmail).to.match(/http\:\/\/example\.com\/auth\/activate\?token\=[a-z0-9]{64}\&email\=test@jksdua.asiaa/);

					mailSpy.restore();

					// address bug where pass was being appended to the user object
						// yep it happended!
					co(function *() {
						var user = yield auth.db.findByEmail(res.body.email);
						expect(user).to.not.have.property('pass');
					}).then(done, done);
				});
			});

			it('should register user', function(done) {
				var args = { email: dummyUser().email + 'A', pass: 'crazy ass passpharse' };
				var mailSpy = helpers.spyOnAsyncMethod(helpers.auth.options.mail.transport, 'send');

				request({
					url: route.register.url,
					method: route.register.method,
					json: args
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.statusCode).to.equal(200);
					expect(mailSpy.history).to.have.length(1);
					expect(mailSpy.history[0].res[1].envelope.to).to.eql(['test@jksdua.asiaa']);
					var rawEmail = mailSpy.history[0].res[1].response.toString();
					expect(rawEmail).to.match(/http\:\/\/example\.com\/auth\/activate\?token\=[a-z0-9]{64}\&email\=test@jksdua.asiaa/);

					mailSpy.restore();

					co(function *() {
						var user = yield auth.db.findByEmail(res.body.email);
						expect(user.email).to.equal('test@jksdua.asiaa');
					}).then(done, done);
				});
			});

			it('should register user and send different mail options', function(done) {
				var args = {
					email: dummyUser().email + 'a',
					pass: 'crazy ass passpharse',
					mailOptions: {
						mail: {
							subject: 'You are invited'
						}
					}
				};
				var mailSpy = helpers.spyOnAsyncMethod(helpers.auth.options.mail.transport, 'send');

				request({
					url: route.register.url,
					method: route.register.method,
					json: args
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.statusCode).to.equal(200);
					expect(mailSpy.history).to.have.length(1);
					expect(mailSpy.history[0].res[1].envelope.from).to.equal('default@example.com');
					expect(mailSpy.history[0].res[1].envelope.to).to.eql(['test@jksdua.asiaa']);
					var rawEmail = mailSpy.history[0].res[1].response.toString();
					expect(rawEmail).to.contain('Subject: You are invited');
					expect(rawEmail).to.match(/http\:\/\/example\.com\/auth\/activate\?token\=[a-z0-9]{64}\&email\=test@jksdua.asiaa/);

					mailSpy.restore();

					// address bug where pass was being appended to the user object
						// yep it happended!
					co(function *() {
						var user = yield auth.db.findByEmail(res.body.email);
						expect(user).to.not.have.property('pass');
					}).then(done, done);
				});
			});
		});

		describe('#activate', function() {
			var assertionUser;

			function user() {
				var u = helpers.dummyUsers()[0];
				u.email = 'a@abc.com';
				// must be 64 characters
				u.activationToken = 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa';
				u.status = helpers.auth.constants.STATUS.REGISTERED;
				return u;
			}

			beforeEach(function(done) {
				assertionUser = user();

				co(function *() {
					yield db.insert(assertionUser);
				}).then(done, done);
			});

			it('should fail if email is invalid', function(done) {
				request({
					url: route.activate.url,
					method: route.activate.method,
					json: { email: user().email + 'a', token: user().activationToken }
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.statusCode).to.equal(400);
					expect(res.body.error).to.equal(auth.constants.MESSAGE.activate.INVALID_EMAIL);

					done();
				});
			});

			it('should fail if token doesnt match', function(done) {
				request({
					url: route.activate.url,
					method: route.activate.method,
					json: { email: user().email, token: user().activationToken.replace(/a/g, 'b') }
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.statusCode).to.equal(400);
					expect(res.body.error).to.equal(auth.constants.MESSAGE.activate.INVALID_TOKEN);

					done();
				});
			});

			it('should activate the user', function(done) {
				request({
					url: route.activate.url,
					method: route.activate.method,
					json: { email: user().email, token: user().activationToken }
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.statusCode).to.equal(200);

					// expected user record
					var u = auth.cleanse(JSON.parse(JSON.stringify(assertionUser)));
					u.status = 'enabled';

					expect(res.body).to.eql(u);

					co(function *() {
						var dbUser = yield db.findByEmail(user().email);

						var u2 = JSON.parse(JSON.stringify(assertionUser));
						u2.status = 'enabled';
						u2.activationToken = null;
						expect(JSON.parse(JSON.stringify(dbUser))).to.eql(u2);
					}).then(done, done);
				});
			});

			it('should activate the user without an activation token', function(done) {
				request({
					url: route.activate.url,
					method: route.activate.method,
					json: { email: user().email, skipTokenVerification: true }
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.statusCode).to.equal(200);

					// expected user record
					var u = auth.cleanse(JSON.parse(JSON.stringify(assertionUser)));
					u.status = 'enabled';

					expect(res.body).to.eql(u);

					co(function *() {
						var dbUser = yield db.findByEmail(user().email);

						var u2 = JSON.parse(JSON.stringify(assertionUser));
						u2.status = 'enabled';
						u2.activationToken = null;
						expect(JSON.parse(JSON.stringify(dbUser))).to.eql(u2);
					}).then(done, done);
				});
			});
		});

		describe('#resendActivationEmail', function() {
			var assertionUser;

			function user() {
				var u = helpers.dummyUsers()[0];
				u.email = 'a@abc.com';
				// must be 64 characters
				u.activationToken = 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa';
				u.status = helpers.auth.constants.STATUS.REGISTERED;
				return u;
			}

			function insertUser(overrides, done) {
				assertionUser = merge(user(), { email: 'bla@bla.com' }, overrides);

				co(function *() {
					assertionUser = yield auth.methods.insert(assertionUser);
				}).then(done, done);
			}

			beforeEach(function(done) {
				insertUser(null, done);
			});

			it('should fail if email is not valid', function(done) {
				var args = { email: 'a' + assertionUser.email };

				request({
					url: route.resendActivationEmail.url,
					method: route.resendActivationEmail.method,
					json: args
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.statusCode).to.equal(400);
					//expect(res.body.error).to.equal();

					done();
				});
			});

			it('should fail if user is already activated', function(done) {
				assertionUser.status = helpers.auth.constants.STATUS.ENABLED;

				co(function *() {
					yield auth.methods.update(assertionUser);
				}).then(function() {
					var args = { email: assertionUser.email };

					request({
						url: route.resendActivationEmail.url,
						method: route.resendActivationEmail.method,
						json: args
					}, function(err, res) {
						expect(err).to.not.exist; // jshint ignore:line
						expect(res.statusCode).to.equal(400);
						expect(res.body.error).to.contain('account has already been activated');
						done();
					});
				}, done);
			});

			it('should resend the activation email', function(done) {
				var args = { email: assertionUser.email };

				request({
					url: route.resendActivationEmail.url,
					method: route.resendActivationEmail.method,
					json: args
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.statusCode).to.equal(200);
					expect(res.body).to.have.property('success', true);

					done();
				});
			});
		});

		describe('#invite', function() {
			it('should fail if email is not valid', function(done) {
				var args = { email: 'abc' };

				request({
					url: route.invite.url,
					method: route.invite.method,
					json: args
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.statusCode).to.equal(400);
					//expect(res.body.error).to.equal();

					done();
				});
			});

			it('should not allow duplicate emails', function(done) {
				var args = { email: dummyUser().email };

				request({
					url: route.invite.url,
					method: route.invite.method,
					json: args
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.statusCode).to.equal(400);
					expect(res.body.error).to.equal(auth.constants.MESSAGE.register.DUPLICATE_EMAIL);

					done();
				});
			});

			it('should invite user', function(done) {
				var args = { email: dummyUser().email + 'a' };
				var mailSpy = helpers.spyOnAsyncMethod(helpers.auth.options.mail.transport, 'send');

				request({
					url: route.invite.url,
					method: route.invite.method,
					json: args
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.statusCode).to.equal(200);
					expect(mailSpy.history).to.have.length(1);

					expect(mailSpy.history[0].res[1].envelope.from).to.equal('default@example.com');
					expect(mailSpy.history[0].res[1].envelope.to).to.eql(['test@jksdua.asiaa']);
					var rawEmail = mailSpy.history[0].res[1].response.toString();
					expect(rawEmail).to.contain('Subject: You are invited');
					expect(rawEmail).to.match(/http\:\/\/example\.com\/auth\/invite\/accept\?token\=[a-z0-9]{64}\&email\=test@jksdua.asiaa/);

					mailSpy.restore();
					done();
				});
			});
		});

		describe('#resendInvitationEmail', function() {
			var assertionUser;

			function user() {
				var u = helpers.dummyUsers()[0];
				u.email = 'a@abc.com';
				// must be 64 characters
				u.invitationToken = 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa';
				u.status = helpers.auth.constants.STATUS.INVITED;
				return u;
			}

			function insertUser(overrides, done) {
				assertionUser = merge(user(), { email: 'bla@bla.com' }, overrides);

				co(function *() {
					assertionUser = yield auth.methods.insert(assertionUser);
				}).then(done, done);
			}

			beforeEach(function(done) {
				insertUser(null, done);
			});

			it('should fail if email is not valid', function(done) {
				var args = { email: 'a' + assertionUser.email };

				request({
					url: route.resendInvitationEmail.url,
					method: route.resendInvitationEmail.method,
					json: args
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.statusCode).to.equal(400);
					//expect(res.body.error).to.equal();

					done();
				});
			});

			it('should fail if user is already activated', function(done) {
				assertionUser.status = helpers.auth.constants.STATUS.ENABLED;

				co(function *() {
					yield auth.methods.update(assertionUser);
				}).then(function() {
					var args = { email: assertionUser.email };

					request({
						url: route.resendInvitationEmail.url,
						method: route.resendInvitationEmail.method,
						json: args
					}, function(err, res) {
						expect(err).to.not.exist; // jshint ignore:line
						expect(res.statusCode).to.equal(400);
						expect(res.body.error).to.contain('account has already been activated');
						done();
					});
				}, done);
			});

			it('should resend the invitation email', function(done) {
				var args = { email: assertionUser.email };

				request({
					url: route.resendInvitationEmail.url,
					method: route.resendInvitationEmail.method,
					json: args
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.statusCode).to.equal(200);
					expect(res.body).to.have.property('success', true);

					done();
				});
			});
		});

		describe('#acceptInvite', function() {
			var assertionUser;

			function user() {
				var u = helpers.dummyUsers()[0];
				u.email = 'a@abc.com';
				// must be 64 characters
				u.activationToken = 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa';
				u.status = helpers.auth.constants.STATUS.INVITED;
				return u;
			}

			beforeEach(function(done) {
				assertionUser = user();

				co(function *() {
					yield auth.db.insert(assertionUser);
				}).then(done, done);
			});

			it('should fail if email is invalid', function(done) {
				request({
					url: route.acceptInvite.url,
					method: route.acceptInvite.method,
					json: { email: user().email + 'a', pass: '324vsxsdfsdfs', token: user().activationToken }
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.statusCode).to.equal(400);
					expect(res.body.error).to.equal(auth.constants.MESSAGE.activate.INVALID_EMAIL);

					done();
				});
			});

			it('should fail if user is not in the correct state', function(done) {
				request({
					url: route.acceptInvite.url,
					method: route.acceptInvite.method,
					// dummy user is enabled
					json: { email: dummyUser().email, pass: '324vsxsdfsdfs', token: user().activationToken }
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.statusCode).to.equal(400);
					expect(res.body.error).to.equal(auth.constants.MESSAGE.activate.INVALID_STATE);

					done();
				});
			});

			it('should fail if token doesnt match', function(done) {
				request({
					url: route.acceptInvite.url,
					method: route.acceptInvite.method,
					json: { email: user().email, pass: '324vsxsdfsdfs', token: user().activationToken.replace(/a/g, 'b') }
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.statusCode).to.equal(400);
					expect(res.body.error).to.equal(auth.constants.MESSAGE.activate.INVALID_TOKEN);

					done();
				});
			});

			it('should fail if password < 8 characters', function(done) {
				var args = { email: user().email, pass: 'abc', token: user().activationToken };

				request({
					url: route.acceptInvite.url,
					method: route.acceptInvite.method,
					json: args
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.statusCode).to.equal(400);
					//expect(res.body.error).to.equal();

					done();
				});
			});

			it('should activate the user', function(done) {
				request({
					url: route.acceptInvite.url,
					method: route.acceptInvite.method,
					json: { email: user().email, pass: '324vsxsdfsdfs', token: user().activationToken }
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.statusCode).to.equal(200);

					// expected user record
					var u = auth.cleanse(JSON.parse(JSON.stringify(assertionUser)));
					u.status = 'enabled';
					expect(res.body).to.eql(u);

					co(function *() {
						var dbUser = yield db.findByEmail(user().email);
						dbUser.hash = null; // different salt generated so wont match

						var u2 = JSON.parse(JSON.stringify(assertionUser));
						u2.hash = null;
						u2.status = 'enabled';
						u2.activationToken = null;
						expect(JSON.parse(JSON.stringify(dbUser))).to.eql(u2);
					}).then(done, done);
				});
			});

			it('should activate the user without an invite token', function(done) {
				request({
					url: route.acceptInvite.url,
					method: route.acceptInvite.method,
					json: { email: user().email, pass: '324vsxsdfsdfs', skipTokenVerification: true }
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.statusCode).to.equal(200);

					// expected user record
					var u = auth.cleanse(JSON.parse(JSON.stringify(assertionUser)));
					u.status = 'enabled';
					expect(res.body).to.eql(u);

					co(function *() {
						var dbUser = yield db.findByEmail(user().email);
						dbUser.hash = null; // different salt generated so wont match

						var u2 = JSON.parse(JSON.stringify(assertionUser));
						u2.hash = null;
						u2.status = 'enabled';
						u2.activationToken = null;
						expect(JSON.parse(JSON.stringify(dbUser))).to.eql(u2);
					}).then(done, done);
				});
			});
		});

		describe('#rejectInvite', function() {
			var assertionUser;

			function user() {
				var u = helpers.dummyUsers()[0];
				u.email = 'a@abc.com';
				// must be 64 characters
				u.activationToken = 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa';
				u.status = helpers.auth.constants.STATUS.INVITED;
				return u;
			}

			beforeEach(function(done) {
				assertionUser = user();

				co(function *() {
					yield auth.db.insert(assertionUser);
				}).then(done, done);
			});

			it('should fail if email is invalid', function(done) {
				request({
					url: route.rejectInvite.url,
					method: route.rejectInvite.method,
					json: { email: user().email + 'a', token: user().activationToken }
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.statusCode).to.equal(400);
					expect(res.body.error).to.equal(auth.constants.MESSAGE.activate.INVALID_EMAIL);

					done();
				});
			});

			it('should fail if user is not in the correct state', function(done) {
				request({
					url: route.rejectInvite.url,
					method: route.rejectInvite.method,
					// dummy user is enabled
					json: { email: dummyUser().email, token: user().activationToken }
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.statusCode).to.equal(400);
					expect(res.body.error).to.equal(auth.constants.MESSAGE.activate.INVALID_STATE);

					done();
				});
			});

			it('should fail if token doesnt match', function(done) {
				request({
					url: route.rejectInvite.url,
					method: route.rejectInvite.method,
					json: { email: user().email, token: user().activationToken.replace(/a/g, 'b') }
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.statusCode).to.equal(400);
					expect(res.body.error).to.equal(auth.constants.MESSAGE.activate.INVALID_TOKEN);

					done();
				});
			});

			it('should remove the user from the database', function(done) {
				request({
					url: route.rejectInvite.url,
					method: route.rejectInvite.method,
					json: { email: user().email, token: user().activationToken }
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.statusCode).to.equal(200);

					// expected user record to be removed
					expect(res.body).to.eql({ success: true });

					co(function *() {
						var dbUser = yield db.findByEmail(user().email);
						expect(dbUser).to.not.exist; // jshint ignore:line
					}).then(done, done);
				});
			});

			it('should remove the user from the database without an invite token', function(done) {
				request({
					url: route.rejectInvite.url,
					method: route.rejectInvite.method,
					json: { email: user().email, skipTokenVerification: true }
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.statusCode).to.equal(200);

					// expected user record to be removed
					expect(res.body).to.eql({ success: true });

					co(function *() {
						var dbUser = yield db.findByEmail(user().email);
						expect(dbUser).to.not.exist; // jshint ignore:line
					}).then(done, done);
				});
			});
		});

		describe('#getProperties', function() {
			it('should return properties', function(done) {
				request({
					url: route.getProperties.url,
					method: route.getProperties.method,
					json: { email: dummyUser().email }
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.body).to.eql(dummyUser().properties);

					done();
				});
			});
		});

		describe('#setProperties', function() {
			it('should update properties', function(done) {
				request({
					url: route.setProperties.url,
					method: route.setProperties.method,
					json: {
						email: dummyUser().email,
						properties: {
							name: { first: 'Test', last: 'User' },
							// for testing purposes
							__pass: 'I like big hashes and I cannot lie',
							a: 'a',
							b: [1]
						}
					}
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.statusCode).to.equal(200);

					request({
						url: route.getProperties.url,
						method: route.getProperties.method,
						json: { email: dummyUser().email }
					}, function(err, res) {
						expect(err).to.not.exist; // jshint ignore:line
						expect(res.statusCode).to.equal(200);
						expect(res.body).to.eql({
							a: 'a',
							b: [1],
							name: { first: 'Test', last: 'User' },
							__pass: 'I like big hashes and I cannot lie'
						});

						// dont override existing properties
						expect(dummyUser()).to.have.property('hash');

						done();
					});
				});
			});
		});

		describe('#changePassword', function() {
			it('should fail if user cant be found', function(done) {
				request({
					url: route.changePassword.url,
					method: route.changePassword.method,
					json: { email: dummyUser().email + 'a', oldPass: dummyUser().properties.__pass, newPass: dummyUser().properties.__pass + 'a' }
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.statusCode).to.equal(400);
					expect(res.body.error).to.equal(auth.constants.MESSAGE.changePassword.INVALID_USER_ID);

					done();
				});
			});

			it('should fail if current password does not match', function(done) {
				request({
					url: route.changePassword.url,
					method: route.changePassword.method,
					json: { email: dummyUser().email, oldPass: dummyUser().properties.__pass + 'a', newPass: dummyUser().properties.__pass }
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.statusCode).to.eql(400);
					expect(res.body.error).to.equal(auth.constants.MESSAGE.changePassword.INVALID_CURRENT_PASSWORD);

					done();
				});
			});

			it('should fail if new password does not meet password criteria', function(done) {
				request({
					url: route.changePassword.url,
					method: route.changePassword.method,
					json: { email: dummyUser().email, oldPass: dummyUser().properties.__pass, newPass: 'a' }
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.statusCode).to.eql(400);
					//expect(res.body.error).to.equal(auth.constants.MESSAGE.changePassword.INVALID_NEW_PASSWORD);

					done();
				});
			});

			it('should work', function(done) {
				request({
					url: route.changePassword.url,
					method: route.changePassword.method,
					json: { email: dummyUser().email, oldPass: dummyUser().properties.__pass, newPass: dummyUser().properties.__pass + 'a' }
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.statusCode).to.eql(200);

					request({
						url: route.login.url,
						method: route.login.method,
						json: { email: dummyUser().email, pass: dummyUser().properties.__pass + 'a' }
					}, function(err, res) {
						expect(err).to.not.exist; // jshint ignore:line
						expect(res.statusCode).to.equal(200);

						done();
					});
				});
			});
		});

		describe('#resetRequest', function() {
			it('should fail if email is invalid', function(done) {
				request({
					url: route.resetRequest.url,
					method: route.resetRequest.method,
					json: { email: dummyUser().email + 'a' }
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.statusCode).to.equal(400);
					expect(res.body.error).to.equal(auth.constants.MESSAGE.resetRequest.INVALID_EMAIL);

					done();
				});
			});

			it('should send password reset email with link', function(done) {
				var mailSpy = helpers.spyOnAsyncMethod(helpers.auth.options.mail.transport, 'send');

				request({
					url: route.resetRequest.url,
					method: route.resetRequest.method,
					json: { email: dummyUser().email }
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.statusCode).to.equal(200);

					expect(mailSpy.history).to.have.length(1);

					expect(mailSpy.history[0].res[1].envelope.from).to.equal('default@example.com');
					expect(mailSpy.history[0].res[1].envelope.to).to.eql(['test@jksdua.asia']);
					var rawEmail = mailSpy.history[0].res[1].response.toString();
					expect(rawEmail).to.contain('Subject: Reset Your account password');
					expect(rawEmail).to.match(/http\:\/\/example\.com\/auth\/reset\?token\=[a-z0-9]{64}\&email\=test@jksdua.asia/);

					mailSpy.restore();
					done();
				});
			});
		});

		describe('#resetResponse', function() {
			const TOKEN = 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa';

			it('should fail if email is invalid', function(done) {
				request({
					url: route.resetResponse.url,
					method: route.resetResponse.method,
					json: { email: dummyUser().email + 'a', token: TOKEN, pass: dummyUser().properties.__pass }
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.statusCode).to.equal(400);
					expect(res.body.error).to.equal(auth.constants.MESSAGE.resetResponse.INVALID_EMAIL);

					done();
				});
			});

			it('should fail if token doesnt exist', function(done) {
				request({
					url: route.resetResponse.url,
					method: route.resetResponse.method,
					json: { email: dummyUser().email, token: TOKEN, pass: dummyUser().properties.__pass }
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.statusCode).to.equal(400);
					expect(res.body.error).to.equal(auth.constants.MESSAGE.resetResponse.INVALID_TOKEN);

					done();
				});
			});

			it('should fail if token is invalid', function(done) {
				var u = dummyUser();
				u.resetToken = TOKEN;
				u.resetExpiry = new Date(Date.now() + 1000000);

				co(function *() {
					yield db.update(u);

					var res = (yield requestThunk({
						url: route.resetResponse.url,
						method: route.resetResponse.method,
						json: { email: dummyUser().email, token: TOKEN.replace(/a/g, 'b'), pass: dummyUser().properties.__pass }
					}))[0];

					expect(res.statusCode).to.equal(400);
					expect(res.body.error).to.equal(auth.constants.MESSAGE.resetResponse.INVALID_TOKEN);
				}).then(done, done);
			});

			it('should fail if token is expired', function(done) {
				var u = dummyUser();
				u.resetToken = TOKEN;
				u.resetExpiry = new Date(Date.now() - 1);

				co(function *() {
					yield db.update(u);

					var res = (yield requestThunk({
						url: route.resetResponse.url,
						method: route.resetResponse.method,
						json: { email: dummyUser().email, token: TOKEN, pass: dummyUser().properties.__pass }
					}))[0];

					expect(res.statusCode).to.equal(400);
					expect(res.body.error).to.equal(auth.constants.MESSAGE.resetResponse.EXPIRED_TOKEN);
				}).then(done, done);
			});

			it('should fail if password does not match criteria', function(done) {
				var u = dummyUser();
				u.resetToken = TOKEN;
				u.resetExpiry = Date.now() + 1000000;

				co(function *() {
					yield db.update(u);

					var res = (yield requestThunk({
						url: route.resetResponse.url,
						method: route.resetResponse.method,
						json: { email: dummyUser().email, token: TOKEN, pass: 'a' }
					}))[0];

					expect(res.statusCode).to.equal(400);
					//expect(res.body.error).to.equal(auth.constants.MESSAGE.resetResponse.INVALID_NEW_PASSWORD);
				}).then(done, done);
			});

			it('should reset the password', function(done) {
				var u = dummyUser();
				u.resetToken = TOKEN;
				u.resetExpiry = Date.now() + 1000000;

				co(function *() {
					yield db.update(u);

					var res = (yield requestThunk({
						url: route.resetResponse.url,
						method: route.resetResponse.method,
						json: { email: dummyUser().email, token: TOKEN, pass: dummyUser().properties.__pass + 'a' }
					}))[0];

					expect(res.statusCode).to.equal(200);

					var res2 = (yield requestThunk({
						url: route.login.url,
						method: route.login.method,
						json: { email: dummyUser().email, pass: dummyUser().properties.__pass + 'a' }
					}))[0];

					expect(res2.statusCode).to.equal(200);

					var u2 = cleansedDummyUser();
					expect(res2.body).to.eql(u2);
				}).then(done, done);
			});

			it('should reset the password without a reset token', function(done) {
				var u = dummyUser();
				u.resetToken = TOKEN;
				u.resetExpiry = Date.now() + 1000000;

				co(function *() {
					yield db.update(u);

					var res = (yield requestThunk({
						url: route.resetResponse.url,
						method: route.resetResponse.method,
						json: {
							email: dummyUser().email,
							pass: dummyUser().properties.__pass + 'a',
							skipTokenVerification: true
						}
					}))[0];

					expect(res.statusCode).to.equal(200);

					var res2 = (yield requestThunk({
						url: route.login.url,
						method: route.login.method,
						json: { email: dummyUser().email, pass: dummyUser().properties.__pass + 'a' }
					}))[0];

					expect(res2.statusCode).to.equal(200);

					var u2 = cleansedDummyUser();
					expect(res2.body).to.eql(u2);
				}).then(done, done);
			});
		});

		describe('#disable', function() {
			it('should work', function(done) {
				request({
					url: route.disable.url,
					method: route.disable.method,
					json: { email: dummyUser().email }
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.statusCode).to.equal(200);

					// dont override existing properties
					expect(dummyUser()).to.have.property('hash');

					done();
				});
			});
		});

		describe('#enable', function() {
			it('should work', function(done) {
				request({
					url: route.enable.url,
					method: route.enable.method,
					json: { email: dummyUser().email }
				}, function(err, res) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(res.statusCode).to.equal(200);

					// dont override existing properties
					expect(dummyUser()).to.have.property('hash');

					done();
				});
			});
		});
	});
});
