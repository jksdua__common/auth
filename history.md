# Changelog

## v7.0.0 (19 July 2016)

- Upgraded mail dependency

## v6.1.2 (7 December 2015)

- Commit issue

## v6.1.1 (7 December 2015)

- Make `email` case insensitive

## v6.1.0 (13 October 2015)

- Added `resendInvitationEmail()` method
- Added option to override mail options using `mailOptions` arg
- Added better error handling when activating account

## v6.0.4 (29 July 2015)
- Minor bugfix to not throw 500 if password is missing in `register`, `login` etc

## v6.0.3 (23 July 2015)
- Minor fixes for making `auth-mongodb` tests pass

## v6.0.2 (23 July 2015)
- Regression fix

## v6.0.1 (23 July 2015)
- Minor bugfixes

## v6.0.0 (22 July 2015)
- Changed name to **@knoxxnxt/auth**
- Added new method `resendActivationEmail`
- Updated mail dependency

## v5.0.4 (15 July 2015)
- Fixed bug in memorydb failing insert `_id` assertion for first record
- Updated dependencies

## v5.0.3 (22 May 2015)
- Fixed `thunkify` broken dependency
- Added validate script

## v5.0.2 (22 May 2015)
- Fixed mail subject
- Fixed password complexity rules to be customisable by the user

## v5.0.1 (23 April 2015)
- **CRITICAL**: Fixed bug where default memory store did not work

## v5.0.0 (13 February 2015)
- **New features**:
  - **BREAKING CHANGE**: Response structure has changed. Methods that used to return `res.status` now return `res.success` instead.
  - **BREAKING CHANGE**: Using password complexity library to disallow weak passwords
  - **BREAKING CHANGE**: Split `.properties()` into `.getProperties()` and `.setProperties()`
  - Added support for `roles` to be optionally provided with register
  - Added support for default `properties` and `roles` added for each new user
    - Existing users need to be updated manually
  - Added raw method `.update()` for directly modifying the user object
  - Added a repl for admin tasks
    - It has the same API signature except methods are not `yielded`, simply call them as a syncrhonous function.
  - Changed structure of mail urls to use a template allowing arbitrary urls
  - Added `debug` to make it easier to debug `js_auth`
  - Added option to skip token verification - useful for admin operations

## v4.0.6 (12 February 2015)
- **CRITICAL**: Fixed bug where `pass` is stored in the database

## v4.0.5 (12 February 2015)
- **CRITICAL**: Fixed bug where `to` repecient was not specified when sending a mail

## v4.0.4 (11 February 2015)
- **CRITICAL**: Fixed bug where existing properties were being overwritten by certain methods

## v4.0.3 (11 February 2015)
- Fixed test bug

## v4.0.2 (11 February 2015)
- Fixed test bug
  - Removed user may be null or undefined
  - When activating, a different salt is generated from the one used in the assertion
- Moved changelog out of `readme.md`

## v4.0.1 (11 February 2015)
- Removed bug where memorydb warning was being printed regardless of whether it was being used

## v4.0.0 (28 January 2015)
- Updated defaults to make it easier to create non-production auth instances
- Added support for basic roles
  - Multiple roles can be assigned to a user
  - Each user is assigned a `role` property with `user` role by default
  - `ROLE` and `ROLES` constants have been added with two basic roles
- Invitations are now allowed. A user may either self-register or get invited into the system. Upon accepting the invitation, the user sets their own password
  - Added `invite`, `acceptInvite` and `rejectInvite` methods
  - URL sent to user for accepting invites has been added in the options hash
  - Added `invite` mail template
- All constants are now exposed by the auth service. `MESSAGE` and `STATUS` are no longer exposed, they can be found under `constants`.
- Added low-level `insert` and `remove` functions
  - Data stores are now required to support an additional method `removeByEmail`.
- Arguments schema has been changed. Arguments passed to methods must not pass additional properties.

## v3.0.0 (22 January 2015)
- Changed api signature for methods that used `_id`. `email` is now exclusively used for user identification publicly.

## v2.0.0 (21 January 2015)
- Changed user model structure

## v1.0.0 (9 December 2014)
- Initial commit
